let autos = [
  {
    marca: "Ford ",
    modelo: "Fiesta",
    precio: 150000,
    km: 200,
    color: "Azul",
    cuotas: 12,
    anio: 2019,
    patente: "APL123",
    vendido: false,
  },
  {
    marca: "Toyota ",
    modelo: "Corolla",
    precio: 100000,
    km: 0,
    color: "Blanco",
    cuotas: 14,
    anio: 2019,
    patente: "JJK116",
    vendido: false,
  },
];

let concesionaria = {
  autos: autos,
  buscarAuto: function (patente) {
    let auto = autos.filter((autos) => autos.patente === patente)[0];
    return auto ? auto : null;
  },

  venderAuto: function (patente) {
    let buscado = concesionaria.buscarAuto(patente);
    buscado.vendido = true;
  },
};

console.log(concesionaria.venderAuto("APL123"));
console.log(concesionaria.buscarAuto("APL123"));
